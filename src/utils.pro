/* -*- Mode:Prolog; coding:iso-8859-1; -*- */

% flatten(+List, -FlatL)
% Transform a list to one dimension

flatten([], []) :- !.

flatten([L|Ls], FlatL) :-
    !,
    flatten(L, NewL),
    flatten(Ls, NewLs),
    append(NewL, NewLs, FlatL).

flatten(L, [L]).

% isZero(+Value, -Bool)
% Check to see if Value is zero.

isZero('0', Bool) :- Bool = yes. 
isZero(0, Bool) :- Bool = yes.
isZero(_, Bool) :- Bool = no.

% writePerRow(+Grid)
% Write out a two-dimensional list one row at a time.

writePerRow([]).

writePerRow([Row|Rest]) :-
        write(Row), nl,
        writePerRow(Rest).