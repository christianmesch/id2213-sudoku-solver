% solve(+Grid, -Solution)
% Solves the sudoku and returns the solution

solve(Grid, Solution) :-
        solve(Grid, 0,0, Solution).        

solve(Grid, 9, 9, Grid) :-!.
        
solve(Grid, Xpos, Ypos, Solution):-
        % Get the next empty cell   
        getNext(Grid, Xpos, Ypos, NewX, NewY),
        
        % Get the possible numbers for the cell
        possibleNumbers(Grid, NewX, NewY, Numbers),
        
        % Fail and backtrack if there are no possible numbers
        Numbers \= [],
        
        % Pop and add a possible number to the grid
        member(Value, Numbers),
        put(Grid, NewX, NewY, Value, [], NewGrid),
        
        % Recursively call for solve
        solve(NewGrid, NewX, NewY, Solution).