% traverse(+List, +N, -X)
% Get the nth element in the list. Index starts with 0.

traverse([X|_], 0, X) :- !.
    
traverse([_|List], N, X) :-
        N1 is N-1,
        traverse(List, N1, X).

% traverse3(+List, +N, -X)
% Get the 3 elements following and including n. Index starts with 0.

traverse3([X1, X2, X3|_], 0, X) :- 
        append([], [X1, X2, X3], X).
    
traverse3([_|List], N, X) :-
        N1 is N-1,
        traverse3(List, N1, X).

% getVertical(+Grid, +Xpos, -Values)
% Get the column on Xpos. Index starts with 0.

getVertical([Row|[]], Xpos, Value):-
        traverse(Row, Xpos, Value).

getVertical([Row|Grid], Xpos, Values) :- 
        traverse(Row, Xpos, Ovalue),
        getVertical(Grid, Xpos, Olist1),
        append([Olist1],[Ovalue],Values).

% getHorizontal(+Grid, +Ypos, -Row)
% Get the row on Ypos. Index starts with 0.

getHorizontal(Grid, Ypos, Values) :-
        traverse(Grid, Ypos, Values).

% getQuadrant(+Grid, +Xpos, +Ypos, -Quadrant)
% Get the quardant in which the the coordinates Xpos and Ypos reside in.

getQuadrant(Grid, Xpos, Ypos, Quadrant) :-
        % Get starting position of quadrant 
        XquadPos is (Xpos // 3) * 3,
        YquadPos is (Ypos // 3) * 3,
        
        % Get rows
        traverse3(Grid, YquadPos, [Y1, Y2, Y3]),
        
        % Get columns
        traverse3(Y1, XquadPos, X1),
        traverse3(Y2, XquadPos, X2),
        traverse3(Y3, XquadPos, X3),
        
        % Create quadrant list
        append(X1, [X2, X3], Tmp),
        flatten(Tmp, Quadrant).


% put(+Grid, +Xpos, +Ypos, +Value, +Head, -NewGrid)
% Put the Value on the coordinates Xpos, Ypos and return the new grid.
putRow([_|List], 0, Value, Head, NewGrid) :-
        append([Value], List, Tmp),
        append(Head, Tmp, NewGrid).

putRow([Col|List], Xpos, Value, Head, NewGrid) :-    
        Xpos1 is Xpos-1,
        append(Head, [Col], Head1),
        putRow(List, Xpos1, Value, Head1, NewGrid).
        
put(Grid, 9, 9, _, _, Grid).

put([Row|List], Xpos, 0, Value, Head, NewGrid) :-
        Xpos < 9,
        putRow(Row, Xpos, Value, [], Row1),
        append([Row1], List, Tmp),
        append(Head, Tmp, NewGrid).
    
put([Row|List], Xpos, Ypos, Value, Head, NewGrid) :-
        Xpos < 9,
        Ypos \= 0,
        Ypos1 is Ypos-1,
        append(Head, [Row], Head1),
        put(List, Xpos, Ypos1, Value, Head1, NewGrid).
        

% isEmpty(+Grid, +Xpos, +Ypos, -Bool)
% Checks if the element on Xpos, Ypos is empty (a zero).

isEmpty(Grid, Xpos, Ypos, Bool) :-
        getHorizontal(Grid, Ypos, Row),
        traverse(Row, Xpos, Oval),
        isZero(Oval, Bool).
        

% getNext(+Grid, +Xpos, +Ypos, -NewX, -NewY)
% Finds and return the coordinates for the next empty element in Grid.

getNext(_, 9, 9, NewX, NewY) :- NewX is 9, NewY is 9.

getNext(Grid, Xpos, Ypos, NewX, NewY) :-
        Xpos < 9,
        isEmpty(Grid, Xpos, Ypos, Obool),
        getNext(Grid, Xpos, Ypos, Obool, NewX, NewY),
        % Cut here so that the backtracking doesn't continue to find new coordinates.
        !.
        
getNext(_, Xpos, Ypos, yes, Xpos, Ypos).

getNext(Grid, Xpos, Ypos, no, NewX, NewY) :-
        increment(Xpos, Ypos, Xpos1, Ypos1),
        getNext(Grid, Xpos1, Ypos1, NewX, NewY),
        % Cut here so that the backtracking doesn't continue to find new coordinates.
        !.



% increment(+Xpos, +Ypos, -NewX, -NewY)
% Return the coordinates to the next cell in the grid.

increment(8, 8, NewX, NewY) :- !, NewX is 9, NewY is 9.

increment(8, Ypos, NewX, NewY) :- 
        Ypos < 8,
        
        % New row
        NewX is 0,
        NewY is Ypos + 1.

increment(Xpos, Ypos, NewX, NewY) :-
        Xpos < 8,
        NewX is Xpos + 1,
        NewY is Ypos.


% possibleNumbers(+Grid, +Xpos, +Ypos, -Numbers)
% Check the coordinates row, column and quadrant and return a list of possible numbers for the cell.

possibleNumbers(Grid, 9, 9, Grid).

possibleNumbers(Grid, Xpos, Ypos, Numbers) :-
        Xpos < 9,
        Ypos < 9,
        
        % Get row.
        getHorizontal(Grid, Ypos, Row),
        
        % Get column.
        getVertical(Grid, Xpos, Col),
        flatten(Col, Col2),
        
        % Get quadrant.
        getQuadrant(Grid, Xpos, Ypos, Quad),
        
        % Get a list with the different numbers it can't be
        union([Row, Col2, Quad], List),
        
        % Remove the undesired numbers
        All = ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        subtract(All, List, Numbers).
















