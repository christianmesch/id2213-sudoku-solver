/* -*- Mode:Prolog; coding:iso-8859-1; -*- */

readfile(Olist) :-
        open('src/file.txt', read, Str),
        readAll(Str, Olist),
        close(Str).

readAll(Str, []) :-
        at_end_of_stream(Str).

readAll(Str, [X|L]) :-      
        \+ at_end_of_stream(Str),
        readChars(Str, A),
        atom_chars(A, X),
        readAll(Str,L).


readChars(Str, Olist) :-
        get_code(Str, Char),
        checkAndRead(Char, Chars, Str),
        atom_codes(Olist, Chars).


checkAndRead(end_of_file,[],_) :- !.
checkAndRead(10,[],_):-  !. 
checkAndRead(32,[],_):-  !. 
checkAndRead(-1,[],_):-  !.

checkAndRead(Char, [Char|Chars], Str) :-
        get_code(Str, NxtChar),
        checkAndRead(NxtChar, Chars, Str).
        