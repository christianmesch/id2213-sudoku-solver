/* -*- Mode:Prolog; coding:iso-8859-1; -*- */
:- use_module(library(system)).
:- use_module(library(sets)).
:- compile('workingdirectory.pro').
:- compile('utils.pro').
:- compile('readfile.pro').
:- compile('sudokuutils.pro').
:- compile('logic.pro').

main :-
        readfile(Grid),
        writePerRow(Grid),nl,
        solve(Grid,Solution),

        writePerRow(Solution).